from Sine_function1 import Sine_function1

def generate_sine_table(n1, n2, n3, f, fs, phi, mag):
    table = []
    for sample_number in range(n1, n3 + 1, n2):
        table.append(Sine_function1(sample_number, f, fs, phi, mag))
    return table

table_result = generate_sine_table(0, 1, 10, 1, 10, 0, 1)
for row in table_result:
    print(row)
