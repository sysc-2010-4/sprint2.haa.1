import numpy as np
from Noise import generatenoise
from graph import plot

def generate_signal(magnitude_sine, magnitude_noise, noise_samples):


    signal1_x = np.arange(0, 2 * np.pi, 0.2)


    sine_wave = magnitude_sine * np.sin(signal1_x)


    noise_signal = magnitude_noise * generatenoise(noise_samples)


    combined_signal = sine_wave + noise_signal

    return signal1_x[:len(combined_signal)], combined_signal

if __name__ == "__main__":
    x_values, signal1 = generate_signal(magnitude_sine=10, magnitude_noise=1, noise_samples=50)
    plot(x_values, signal1, color='blue', marker='.', title='Signal: Sine(10) + Noise(1)')


    x_values, signal2 = generate_signal(magnitude_sine=1, magnitude_noise=1, noise_samples=50)
    plot(x_values, signal2, color='green', marker='.', title='Signal: Sine(1) + Noise(1)')

    x_values, signal3 = generate_signal(magnitude_sine=1, magnitude_noise=10, noise_samples=50)
    plot(x_values, signal3, color='red', marker='.', title='Signal: Sine(1) + Noise(10)')
