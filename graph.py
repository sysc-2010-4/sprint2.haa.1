import matplotlib.pyplot as plt

def plot(x, y, color='blue', marker='o', title='Plot', x_label='X-axis', y_label='Y-axis'):
    """
    Plot the values of x and y.


    """
    plt.plot(x, y, color=color, marker=marker)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.grid(True)
    plt.show()


if __name__ == "__main__":
    x_values = [1, 2, 3, 4, 5]
    y_values = [2, 4, 6, 8, 10]

plot(x_values, y_values, color='green', marker='^', title='Custom Plot', x_label='Custom X-axis', y_label='Custom Y-axis')
