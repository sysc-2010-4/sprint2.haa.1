import numpy as np
from graph import plot

def generate_and_plot_signals(*signals):

    x_values = np.arange(0, 2 * np.pi, 0.01)

    for signal_params in signals:
        frequency, magnitude = signal_params
        sine_wave = magnitude * np.sin(2 * np.pi * frequency * x_values)
        plot(x_values, sine_wave, title=f'Signal: Sin({frequency} Hz), Mag={magnitude}')

# Example usage:
if __name__ == "__main__":

    generate_and_plot_signals((1, 1), (2, 1))
    generate_and_plot_signals((1, 1), (3, 1))
    generate_and_plot_signals((1, 1))
    generate_and_plot_signals((1, 1/3), (3, 1/3))
    generate_and_plot_signals((1, 1/3), (3, 1/3), (5, 1/5))
    generate_and_plot_signals((1, 1/3), (3, 1/3), (5, 1/5), (7, 1/7))
    generate_and_plot_signals((1, 1/3), (3, 1/3), (5, 1/5), (7, 1/7), (9, 1/9))
