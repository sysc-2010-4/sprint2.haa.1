import math
def sine_function(n, f, fs, phi, mag):
    angle = (n * 2 * math.pi * f / fs) + phi
    result = mag * math.sin(angle)
    return result
def driver1():
 #Testing
    n = 100
    f = 440
    fs = 1000
    phi = math.pi / 4
    mag = 1.0

    result = sine_function(n, f, fs, phi, mag)
    print(f"Result for n={n}, f={f}, fs={fs}, phi={phi}, mag={mag}: {result}")

if __name__ == "__main__":
    driver1()
