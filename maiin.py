# graph.py
import matplotlib.pyplot as plt

def plot(x, y, color='blue', marker='o', title='Plot', x_label='X-axis', y_label='Y-axis'):

    plt.plot(x, y, color=color, marker=marker)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.grid(True)
    plt.show()

# Example usage:
if __name__ == "__main__":

    signal1_x = [i for i in range(10)]


    plot(signal1_x, signal1_y, color='green', marker='^', title='Signal 1 Plot', x_label='Time', y_label='Amplitude')

#The difference in the graphs is that the first graph was a straight line and the second graph, the line goes up and then goes down directly.