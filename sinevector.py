import math
import numpy
import matplotlib

def sinearr(n1, n2, n3, f, fs, phi, mag):
    table = []

    for sample_number in range(n1, n3 + 1, n2):
        time_seconds = sample_number / fs
        signal_value = mag * math.sin(2 * math.pi * f * time_seconds + phi)
        table.append([sample_number, time_seconds, signal_value])

    return table

# Example usage:
n1 = 0
n2 = 100
n3 = 1000
f = 10
fs = 1000
phi = 0
mag = 1

sine_table = sinearr(n1, n2, n3, f, fs, phi, mag)

