import numpy as np

def binarygen(noise_signal):

    binary_values = np.where(noise_signal <= 0, 0, 1)
    return binary_values


if __name__ == "__main__":

    noise_signal = generatenoise(50)

    binary_values = binarygen(noise_signal)

    print(binary_values)
