import numpy as np
from Noise import generatenoise
from graph import plot


signal1_x = np.arange(0, 2 * np.pi, 0.2)


noise_signal = generatenoise(50)

plot(signal1_x[:50], noise_signal, color='orange', marker='.', title='Noise Signal', x_label='Time', y_label='Amplitude'
     )
