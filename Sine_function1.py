import math

def sine_function(sample_number, frequency, sampling_frequency, phase_shift, magnitude):
    time_seconds = sample_number / sampling_frequency
    signal_value = magnitude * math.sin(2 * math.pi * frequency * time_seconds + phase_shift)
    return sample_number, time_seconds, signal_value


class Sine_function1:
    pass